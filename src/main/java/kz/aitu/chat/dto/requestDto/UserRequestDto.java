package kz.aitu.chat.dto.requestDto;

import lombok.Data;

@Data
public class UserRequestDto {
    private Long user_id;
    private String name;
    private String login;
    private String password;
    private String token;
}
