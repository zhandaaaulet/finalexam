package kz.aitu.chat.model;


import kz.aitu.chat.dto.requestDto.UserRequestDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "auth")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Auth {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String login;
    private String password;
    private Long user_id;
    private String token;


    public Auth(UserRequestDto userRequestDto) {
        this.login = userRequestDto.getLogin();
        this.password = userRequestDto.getPassword();
        this.user_id = userRequestDto.getUser_id();
        this.token = userRequestDto.getToken();
    }
}
