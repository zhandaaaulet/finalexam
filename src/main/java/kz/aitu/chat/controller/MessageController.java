package kz.aitu.chat.controller;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.Message;
import kz.aitu.chat.service.AuthService;
import kz.aitu.chat.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/message")
@AllArgsConstructor
public class MessageController {
    private final MessageService messageService;
    private final AuthService authService;


    @GetMapping("chat/{chatId}/{size}")
    public ResponseEntity<?> findLastMessagesByChatId(@PathVariable Long chatId, Integer size) {
        return ResponseEntity.ok(messageService.findLastMessagesByChatId(chatId, size));
    }

    @PostMapping("chat/add")
    public ResponseEntity<?> addMessage(@RequestBody Message message, @RequestHeader("auth") String token) throws Exception {
        Auth auth = authService.getByToken(token);
        if (auth == null) {
            return ResponseEntity.badRequest().body("Token is invalid");
        }
        return ResponseEntity.ok(messageService.addMessage(message));
    }

    @PutMapping("chat/update")
    public ResponseEntity<?> updateMessage(@RequestBody Message message, @RequestHeader("auth") String token) throws Exception {
        Auth auth = authService.getByToken(token);
        if (auth == null) {
            return ResponseEntity.badRequest().body("Token is invalid");
        }
        return ResponseEntity.ok(messageService.updateMessage(message));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteMessageById(@PathVariable Long id) {
        messageService.deleteMessage(id);
        return ResponseEntity.noContent().build();
    }

}
