package kz.aitu.chat.controller;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.Chat;
import kz.aitu.chat.model.Message;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.service.AuthService;
import kz.aitu.chat.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/chat")
@AllArgsConstructor
public class ChatController {
    private ChatService chatService;
    private AuthService authService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(chatService.getAll());
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteChatById(@PathVariable(name = "id") Long id) {
        chatService.deleteChatById(id);
        return ResponseEntity.noContent().build();

    }

    @PostMapping("add")
    public ResponseEntity<?> addChat(@RequestBody Chat chat, @RequestHeader("auth") String token) throws Exception {
        Auth auth = authService.getByToken(token);
        if (auth == null) {
            return ResponseEntity.badRequest().body("Token is invalid");
        }
        return ResponseEntity.ok(chatService.addChat(chat));
    }
}
