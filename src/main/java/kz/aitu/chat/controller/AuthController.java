package kz.aitu.chat.controller;


import javassist.NotFoundException;
import kz.aitu.chat.dto.requestDto.UserRequestDto;
import kz.aitu.chat.dto.responseDto.UserResponseDto;
import kz.aitu.chat.model.Auth;
import kz.aitu.chat.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("api/v1/auth")
@AllArgsConstructor
public class AuthController {
    private final AuthService authService;


    @PostMapping("/registration")
    public ResponseEntity<?> applicantRegistration(@RequestBody UserRequestDto userRequestDto) throws Exception {
        Auth createUser = authService.registration(userRequestDto.getLogin(), userRequestDto.getPassword(), userRequestDto.getName());
        return ResponseEntity.ok(createUser);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody UserRequestDto userRequestDto) throws NotFoundException {
        return ResponseEntity.ok(authService.authorization(userRequestDto.getLogin(), userRequestDto.getPassword()));
    }
}
