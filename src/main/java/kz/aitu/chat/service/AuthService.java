package kz.aitu.chat.service;

import javassist.NotFoundException;
import kz.aitu.chat.dto.requestDto.UserRequestDto;
import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.repository.AuthRepository;
import kz.aitu.chat.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {
    private final AuthRepository authRepository;
    private final UsersRepository usersRepository;

    public Auth registration(String login, String password, String name) throws Exception {
        Auth user = authRepository.getByLoginAndPassword(login, password);
        if (user != null) {
            throw new Exception("This login is already exist!");
        } else {
            UserRequestDto createUser = new UserRequestDto();
            Users newUser = new Users();
            Long date = new Date().getTime();

            newUser.setName(name);
            newUser = usersRepository.save(newUser);

            createUser.setUser_id(newUser.getId());
            createUser.setLogin(login);
            createUser.setPassword(password);
            return authRepository.save(new Auth(createUser));
        }
    }

    public String authorization(String login, String password) throws NotFoundException {
        Auth user = authRepository.getByLoginAndPassword(login, password);

        if (user != null) {
            UUID token = UUID.randomUUID();
            user.setToken(token.toString());
            authRepository.save(user);
            return user.getToken();
        }
        throw new NotFoundException("User not found by username: " + login);
    }

    public Auth getByToken(String token) throws Exception {
        try {
            return authRepository.findByToken(token);
        } catch (Exception e) {
            throw new Exception("Not found token!");
        }
    }


}
