package kz.aitu.chat.service;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;


@Service
@AllArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;


    public List<Message> findLastMessagesByChatId(Long chatId, Integer size) {
        if (size == 0) size = 10;

        return messageRepository.findAllByChatId(chatId, PageRequest.of(1, size, Sort.by(Sort.Direction.DESC, "createdTimestamp")).first());
    }

    public Message addMessage(Message message) throws Exception {
        System.out.println(message);
        if (message == null) throw new Exception("Message is null!");
        if (message.getId() != null) throw new Exception("Message has id!");
        return messageRepository.save(message);
    }

    public void deleteMessage(Long messageId) {
        messageRepository.deleteById(messageId);
    }

    public Message updateMessage(Message message) throws Exception {
        if (message.getId() == null) throw new Exception("Message id is null!");
        if (message.getText() == null || message.getText().isBlank()) throw new Exception("Message is blank!");
        Optional<Message> messageDB = messageRepository.findById(message.getId());
        if (messageDB.isEmpty()) throw new Exception("Message not found!");
        String text = message.getText();
        message = messageDB.get();
        message.setText(text);
        return messageRepository.save(message);
    }


    public List<Message> getAll() {
        return messageRepository.findAll();
    }

    public Message getMessageById(Long id) {
        return messageRepository.findById(id).get();
    }
    
}
